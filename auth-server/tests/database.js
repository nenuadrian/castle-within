const sqlite3 = require('sqlite3').verbose();
const async = require('async');

async function getConnection(logger) {
    return new Promise((callback) => {
        var fs = require("fs")

        var contents = fs.readFileSync('./db/structure.sql', 'utf8').split("\n\n");

        let db = new sqlite3.Database(':memory:', (err) => {
            if (err) {
                logger.error(err.message);
            }
            logger.info('Connected to the database.');
        });

        let tasks = contents.map(sql => {
            return (cb) => db.run(sql, cb)
        })

        async.series(tasks, (err, res) => {
            if (err) logger.error(err)
            logger.info(res)
            logger.info('done db init')
            callback(db)
        })
    })
}
 
module.exports = getConnection
