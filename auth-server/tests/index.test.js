const request = require('supertest')

console.log('Testing')

async function createAccount(app) {
  return request(app)
    .post('/signup')
    .send({
      username: 'test2',
      password: 'test2'
    })
}

  describe('Post Endpoints', () => {
    var conn
    var app 

    it('should connect to db', async () => {
      conn = await require('./database')()
      app = require('../server').server(conn)
    })

      it('should fail login', async () => {
      
          const res = await request(app)
            .post('/login')
            .send({
              username: 'test'
            })
  
          expect(res.body.status).toEqual(500)
        })

      

      it('should create an account', async () => {
        let res = await createAccount(app)
        expect(res.body.status).toEqual(200)
      })

      it('should not fail login', async () => {
        const res = await request(app)
          .post('/login')
          .send({
            username: 'test2',
            password: 'test2'
          })
          expect(res.body.status).toEqual(200)

        const res2 = await request(app)
          .post('/validate')
          .send({
            session_hash: res.body.session_hash
          })
        expect(res2.body.status).toEqual(200)
      })
  })
