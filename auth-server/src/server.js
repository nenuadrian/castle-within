
const express = require('express')
const app = express()
const bcrypt = require('bcrypt');
const saltRounds = 10;
const uuidv1 = require('uuid/v1');

function server(connection) {
    var bodyParser = require('body-parser')
    app.use( bodyParser.json() );
    app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
        extended: true
    })); 

    app.post('/login', (req, res) => {
        connection.all('SELECT * FROM user WHERE username = ? LIMIT 1', [req.body.username], function (error, results) {
            if (error) throw error;
            if (results.length === 0) {
                res.json({ status: 500 })
            } else {
                bcrypt.compare(req.body.password, results[0].password, function(err, r) {
                    if (!r) {
                        res.json({ status: 500 })
                    } else {
                        var session_hash = uuidv1() 
                        connection.run('INSERT INTO user_session(user_id, session_hash) values(?, ?)', [ results[0].user_id, session_hash ], function (error, results, fields) {
                            if (error) throw error;
                            res.json({ status: 200, session_hash: session_hash })
                        });
                    }
                });
            }
        });
    })

    app.post('/validate', (req, res) => {
        connection.all('SELECT * FROM user_session WHERE session_hash = ? LIMIT 1', [req.body.session_hash], function (error, results) {
            if (error) throw error;
            if (results.length === 0) {
                res.json({ status: 500 })
            } else {
                res.json({ status: 200 })
            }
        });
    })

    app.post('/signup', (req, res) => {
        connection.all('SELECT * FROM user WHERE username = ? LIMIT 1', [req.body.username], function (error, results) {
            if (error) throw error;
            if (results.length !== 0) {
                res.json({ status: 500, message: 'USERNAME_IN_USE' })
            } else {
                bcrypt.genSalt(saltRounds, function(err, salt) {
                    bcrypt.hash(req.body.password, salt, function(err, hash) {
                        connection.run('INSERT INTO user (username, password) values (?, ?) ', [req.body.username, hash], function (error, results, fields) {
                            if (error) throw error;
                            res.json({ status: 200 })
                        });
                    });
                })
                
            }
        })
    })

    return app
}
module.exports = {
    server: server
};
