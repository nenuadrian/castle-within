

const winston = require('winston');

(async function main() {
  const logger = winston.createLogger({
    level: 'info',
    format: winston.format.json(),
    defaultMeta: { service: 'login-service' },
    transports: [
      //
      // - Write to all logs with level `info` and below to `combined.log` 
      // - Write all logs error (and below) to `error.log`.
      //
      //new winston.transports.File({ filename: 'error.log', level: 'error' }),
     // new winston.transports.File({ filename: 'combined.log' })
     new winston.transports.Console({
        format: winston.format.simple()
      })
    ]
  });

  let connection = await require('../tests/database')(logger);
  var app = require('./server').server(logger, connection)
  const port = 3000

  app.listen(port, () => logger.info(`Login server listening on port ${port}!`))
  })();
  