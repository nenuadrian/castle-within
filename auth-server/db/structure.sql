CREATE TABLE `user` (
  `user_id` INTEGER PRIMARY KEY AUTOINCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(512) NOT NULL
);


CREATE TABLE `user_session` (
  `session_id` INTEGER PRIMARY KEY AUTOINCREMENT ,
  `session_hash` varchar(512) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);