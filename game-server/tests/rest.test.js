const request = require('supertest')

console.log('Testing')

describe('Rest', () => {
  var conn
  var app 

  it('should connect to db', async () => {
    conn = await require('./database')()
    app = require('../server').server(conn)
  })

  it('should respond with shop listings', async () => {
    const res = await request(app)
      .get('/shopListing/3')

    expect(res.body.status).toEqual(200)
  })

  it('should allow shopping', async () => {
    const res = await request(app)
      .post('/shopListing/testId')
    expect(res.body.status).toEqual(200)
  })

})