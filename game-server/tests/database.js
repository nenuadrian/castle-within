const sqlite3 = require('sqlite3').verbose();
const async = require('async');

async function getConnection() {
    return new Promise((callback) => {
        var fs = require("fs")

        var contents = fs.readFileSync('./db/structure.sql', 'utf8').split("\n\n");

        let db = new sqlite3.Database(':memory:', (err) => {
            if (err) {
                console.error(err.message);
            }
            console.log('Connected to the database.');
        });

        let tasks = contents.map(sql => {
            return (cb) => db.run(sql, cb)
        })

        async.series(tasks, (err, res) => {
            console.log(err)
            console.log(res)
            console.log('done db init')
            callback(db)
        })
    })
}
 
module.exports = getConnection
