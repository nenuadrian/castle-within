
const express = require('express')
const app = express()

var bodyParser = require('body-parser')
app.use( bodyParser.json() );
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
    extended: true
})); 


function server(connection) {

    app.get('/game-data/floors', (req, res) => {
        res.json({ status: 200, data: 100 })
    })

    app.get('/shopListing/:area', (req, res) => {
        res.json({ 
            status: 200,
            data: [
            {
                item: {},
                price: 3
            }
        ]})
    })

    app.post('/shopListing/:itemForSaleId', (req, res) => {
        res.json({ 
            status: 200
        })
    })

    app.get('/game-data/zones/:floor', (req, res) => {
        res.json({ data: [
            { name: 'Zone 1' }
        ]})
    })

    app.get('/game-data/floors/:floor', (req, res) => {
        res.json({ data: 
            { name: 'Floor ' + req.params.floor }
        })
    })

    app.get('/game-data/areas/:zone', (req, res) => {
        res.json({ data: [
            { name: 'Area 1' }
        ]})
    })

    app.get('/game-data/area/:area', (req, res) => {
        res.json({ data: [
            { name: 'Area 1' }
            ],
        })
    })

    app.get('/player/inventory', (req, res) => {
        res.json({ data: [
            {
                itemType: 'potion-1'
            }
        ] })
    })

    app.get('/player/skills', (req, res) => {
        res.json({ data: [
            {
            }
        ] })
    })

    return app
}

module.exports = {
    server: server
};
