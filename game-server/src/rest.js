
(async function main() {
    let connection = await require('../tests/database')();
    var app = require('./server').server(connection)
    const port = 3001

    app.listen(port, () => console.log(`Game server listening on port ${port}!`))

  })();
  