const WebSocket = require('ws');


const wss = new WebSocket.Server({ port: 8080 });

  const handlers = {
    'fight': (ws, data) => {
      
    }
  }

  wss.on('connection', (ws) => {
    ws.on('message', (message) => {
      try {
        const parsed = JSON.parse(message)
        console.log('received: %s', parsed);

        if (!handlers[parsed.action]) throw new Exception('Could not find action %s', parsed.action)

        handlers[parsed.action](ws, parsed.data)

      } catch (error) {
        console.error(error)
      }
    });
  
    ws.send(JSON.stringify({ action: 'hello', data: 'world' }));
  });