
import 'package:flutter/material.dart';
import 'package:hello/player.dart';
import 'package:provider/provider.dart';

import 'inventory.dart';

class InventoryItemTypePage extends StatefulWidget {
  final ItemType itemType;

  InventoryItemTypePage(this.itemType);

  @override
  State<StatefulWidget> createState() {
    return _InventoryItemTypePage(this.itemType);
  }

}

class _InventoryItemTypePage extends State<InventoryItemTypePage> {
  final ItemType itemType;

  _InventoryItemTypePage(this.itemType);

  @override
  Widget build(BuildContext context) {
    return Consumer<PlayerModel>(
      builder: (context, player, child) {
        return Scaffold(
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: Icon(Icons.close),
            backgroundColor: Colors.blue,
          ),
          body: Center(
            // Center is a layout widget. It takes a single child and positions it
            // in the middle of the parent.
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                for (var item in player.items.where((i) => i.itemType == itemType))
                MaterialButton(
                  onPressed: () {
                  },
                  child: Text(item.name)
                  ,
                ),
              ],
            ),
          ),
        );
      }
    );
  }
}


