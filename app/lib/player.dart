

import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'inventory.dart';

class Equipment {
  Item gloves;
  Item headgear;
  Item boots;
  Item armor;
  Item leftHand;
  Item rightHand;
}

class Stats {

}

class Skill {
  int id;
  String name;
  Skill(this.id, this.name);

}

class PlayerModel extends ChangeNotifier {
  final String id;
  final String name;
  final Equipment equipment = new Equipment();
  final Stats stats = new Stats();
  final List<Skill> skills = [
    new Skill(1, "Asssasinate")
  ];
  final List<Item> _items = [];
  UnmodifiableListView<Item> get items => UnmodifiableListView(_items);

  PlayerModel(this.id, this.name);

  void addItem(Item item) {
    _items.add(item);
    notifyListeners();
  }
}


class PlayerPage extends StatefulWidget {

  PlayerPage();
  @override
  _PlayerPage createState() => _PlayerPage();
}


class _PlayerPage extends State<PlayerPage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: InventoryButton(),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(Icons.person_pin, size: 100),
              Icon(Icons.healing),
              Text("Health"),
              
            MaterialButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text("back")
              ,
            )
          ],
        ),
      ),
    );
  }
}



class PlayerButton extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => PlayerPage()),
          );
        },
        child: Icon(Icons.person_pin),
        backgroundColor: Colors.blue,
      );
  }
  
}
