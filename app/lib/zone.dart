

import 'package:flutter/material.dart';
import 'package:hello/floor.dart';

import 'area.dart';
import 'player.dart';


class ZonePage extends StatefulWidget {
  final String zone;
  final String floor;

  ZonePage(this.zone, this.floor);
  @override
  _ZonePage createState() => _ZonePage(zone, floor);
}


class _ZonePage extends State<ZonePage> {
  String zone;
  String floor;

  List<String> _areas = [
    "lake",
    "mansion",
    "shop1",
    "rent-district"
  ];

  _ZonePage(this.zone, this.floor);

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      floatingActionButton: PlayerButton(),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            for (var area in _areas)
            Column(
              children: [
                MaterialButton(
                  onPressed: () {
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(builder: (context) => AreaPage(area, zone, floor)),
                    );
                  },
                  child: Text(area)
                  ,
                ),
              ] 
            ),
            MaterialButton(
              onPressed: () {
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(builder: (context) => FloorPage(floor)),
                );
              },
              child: Text(" to floor")
              ,
            )
          ],
        ),
      ),
    );
  }
}
