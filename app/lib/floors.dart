

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:hello/zone.dart';
import 'package:http/http.dart' as http;

import 'floor.dart';
import 'player.dart';

class FloorsPage extends StatefulWidget {
  @override
  _FloorsPage createState() => _FloorsPage();
}


class _FloorsPage extends State<FloorsPage> {

  @override
  Widget build(BuildContext context) {
    // than having to individually change instances of widgets.
    return Scaffold(
      floatingActionButton: PlayerButton(),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Column(
              children: [
                for(var i in List<int>.generate(10, (i) => i + 1)) 
                  MaterialButton(
                    onPressed: () {
                      Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(builder: (context) => FloorPage(i.toString())),
                      );
                    },
                    child: Text("Floor " + i.toString()
                  ),
                )
              ] 
            )
          ],
        ),
      ),
    );
  }
}
