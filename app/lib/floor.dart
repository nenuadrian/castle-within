

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:hello/zone.dart';
import 'package:http/http.dart' as http;

import 'floors.dart';
import 'player.dart';



class Floor {
  
  final String name;
  final List<String> zones = [
      "forest",
      "ancient city"
    ];
    
  Floor({this.name});

  factory Floor.fromJson(Map<String, dynamic> json) {
    return Floor(
      name: json['name'],
    );
  }
}

Future<Floor> fetchFloor(String floor) async {
    final response =
        await http.get('http://localhost/game-data/floors/' + floor.toString());
  
    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON.
      return Floor.fromJson(json.decode(response.body));
  } else {
    // If that call was not successful, throw an error.
    throw Exception('Failed to load post');
  }
}

class FloorPage extends StatefulWidget {
  final String floor;

  FloorPage(this.floor);

  @override
  _FloorPage createState() => _FloorPage(floor);
}


class _FloorPage extends State<FloorPage> {
  String floorName;
  

  Future<Floor> floor;

    @override
    void initState() {
      super.initState();
      floor = fetchFloor(floorName);
    }


  _FloorPage(this.floorName);

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      floatingActionButton: PlayerButton(),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Column(
              children: [
                FutureBuilder<Floor>(
                  future: floor,
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
            //for (var zone in floor.zones)

                      return Text(snapshot.data.name);
                    } else if (snapshot.hasError) {
                      return Text("${snapshot.error}");
                    }

                    // By default, show a loading spinner.
                    return CircularProgressIndicator();
                  },
                ),

                MaterialButton(
                  onPressed: () {
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(builder: (context) => FloorsPage()),
                    );
                  },
                  child: Text("Floors")
                  ,
                ),
              ] 
            )
          ],
        ),
      ),
    );
  }
}
