
import 'package:flutter/material.dart';
import 'package:hello/player.dart';
import 'package:hello/zone.dart';
import 'package:provider/provider.dart';


class MainMenuPage extends StatefulWidget {
  MainMenuPage({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _MainMenuPageState();
  }

}

class _MainMenuPageState extends State<MainMenuPage> {
  _MainMenuPageState();

  @override
  Widget build(BuildContext context) {
    return Consumer<PlayerModel>(
      builder: (context, player, child) {
        return Scaffold(
          body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                MaterialButton(
                  onPressed: () {
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(builder: (context) => ZonePage("test-zone", "1")),
                    );
                  },
                  child: Text("Enter current zone")
                  ,
                ),
              ],
            ),
          )
        );
      }
    );
  }
}
