
import 'package:flutter/material.dart';
import 'package:hello/main-menu.dart';
import 'package:hello/player.dart';
import 'package:provider/provider.dart';

void main() => runApp(
      ChangeNotifierProvider(
        create: (context) => PlayerModel("sdfsd", "kirito"),
        child: MyApp(),
      ),
);

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Castle Within',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MainMenuPage(),
    );
  }
}
