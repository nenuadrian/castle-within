

import 'package:flutter/material.dart';
import 'package:hello/player.dart';
import 'package:provider/provider.dart';

import 'fight.dart';
import 'standard-shop.dart';
import 'zone.dart';


class AreaPage extends StatefulWidget {
  final String area;
  final String parentZone;
  final String parentFloor;

  AreaPage(this.area, this.parentZone, this.parentFloor);

  @override
  _AreaPage createState() => _AreaPage(area, parentZone, parentFloor);
}


class _AreaPage extends State<AreaPage> {
  final String area;
  final String parentZone;
  final String parentFloor;

  _AreaPage(this.area, this.parentZone, this.parentFloor);

  @override
  Widget build(BuildContext context) {
    return Consumer<PlayerModel>(
      builder: (context, player, child) {
        return  Scaffold(
          floatingActionButton: PlayerButton(),
          body: Center(
            // Center is a layout widget. It takes a single child and positions it
            // in the middle of the parent.
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  area
                ),
                MaterialButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => StandardShopPage()),
                    );
                  },
                  child: Text("SHOP")
                  ,
                ),
                MaterialButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => FightPage()),
                    );
                  },
                  child: Text("FIGHT")
                  ,
                ),
                MaterialButton(
                  onPressed: () {
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(builder: (context) => ZonePage(parentZone, parentFloor)),
                    );
                  },
                  child: Text("Back to zonke")
                  ,
                )
              ],
            ),
          ),
        );
      }
    );
  }
}
