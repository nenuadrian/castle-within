

import 'package:flutter/material.dart';
import 'package:hello/player.dart';
import 'package:provider/provider.dart';
import 'package:web_socket_channel/io.dart';


class FightPage extends StatefulWidget {

  FightPage();

  @override
  _FightPage createState() => _FightPage();
}

class Monster {
  int id = 1;
  String name = "Spider";
  int health = 1000;
}
class _FightPage extends State<FightPage> {

  Monster monster = new Monster();
  final channel = IOWebSocketChannel.connect('ws://localhost:8080');

  _FightPage() {
    channel.stream.listen(_onReceptionOfMessageFromServer);
    
  }
    
  @override
  Widget build(BuildContext context) {
    return Consumer<PlayerModel>(
      builder: (context, player, child) {
        return  Scaffold(
          floatingActionButton: PlayerButton(),
          body: Center(
            // Center is a layout widget. It takes a single child and positions it
            // in the middle of the parent.
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  monster.name
                ),
                Text(
                  "Health:"
                ),
                Text(monster.health.toString()),
                for(var skill in player.skills) 
                  MaterialButton(
                    onPressed: () {
                      channel.sink.add('{ "action": "fight", "data": "monster", "skill": "${skill.id}" }');

                      setState(() {
                        monster.health -= 100;
                      });
                      if (monster.health <= 0) {
                        Navigator.pop(context);
                      }
                    },
                    child: Text(skill.name)
                    ,
                  ),
                MaterialButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text("CLOSE")
                  ,
                )
              ],
            ),
          ),
        );
      }
    );
  }
    
  void _onReceptionOfMessageFromServer(event) {
    print(event);
    setState(() {
      monster.health -= 100;
    });
  }
}
