
import 'package:flutter/material.dart';
import 'package:hello/player.dart';
import 'package:provider/provider.dart';

import 'inventory-item-type.dart';

class InventoryPage extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return _InventoryPage();
  }

}

class Item {
  String name = "Item";
  ItemType itemType = new ItemType("potion");
  int degradation = 0;
  String expiry;

  static fromJson(d) {
    return new Item();
  }
  
}

class ItemType {
  String name = "Item Type";
  ItemType(this.name);
}

class _InventoryPage extends State<InventoryPage> {

  _InventoryPage();

  @override
  Widget build(BuildContext context) {
    return Consumer<PlayerModel>(
      builder: (context, player, child) {
        return Scaffold(
          floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
          floatingActionButton: FloatingActionButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: Icon(Icons.close),
            backgroundColor: Colors.blue,
          ),
          body: Center(
            // Center is a layout widget. It takes a single child and positions it
            // in the middle of the parent.
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                for (var itemType in player.items.map((i) => i.itemType).toSet())
                MaterialButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => InventoryItemTypePage( itemType )),
                    );
                  },
                  child: Text(itemType.name + " x " + player.items.where((i) => i.itemType == itemType).length.toString())
                  ,
                ),
              ],
            ),
          ),
        );
      }
    );
  }
}


class InventoryButton extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => InventoryPage()),
          );
        },
        child: Icon(Icons.toys),
        backgroundColor: Colors.blue,
      );
  }
  
}
