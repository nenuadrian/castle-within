import 'package:flutter/material.dart';
import 'package:hello/inventory.dart';
import 'package:hello/player.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class ItemForSale {
  String id;
  Item item;
  int price;

  ItemForSale(this.item, this.price);
  static Future<List<ItemForSale>> fromJson(decode) async {
    print(decode);
    List<ItemForSale> itemsForSale = [];
    for (var itemForSale in decode['data']) {
      itemsForSale.add(new ItemForSale(Item.fromJson(itemForSale), 3));
    }

    return itemsForSale;
  }
}

Future<List<ItemForSale>> fetchShopListing(int areadId) async {
  final response =
      await http.get('http://localhost:3001/shopListing/$areadId');

  if (response.statusCode == 200) {
    return ItemForSale.fromJson(json.decode(response.body));
  } else {
    // If that call was not successful, throw an error.
    throw Exception('Failed to load');
  }
}

Future<void> buyItem(ItemForSale itemForSale) async {
  await http.post('http://localhost:3001/shopListing/${itemForSale.id}');
  return;
}

class StandardShopPage extends StatefulWidget {

  StandardShopPage();

  @override
  _StandardShopPage createState() => _StandardShopPage();
}

class _StandardShopPage extends State<StandardShopPage> {

  Future<List<ItemForSale>> itemsForSell;

  @override
  void initState() {
    super.initState();
    itemsForSell = fetchShopListing(1);
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<PlayerModel>(
      builder: (context, player, child) {
        return  Scaffold(
          floatingActionButton: PlayerButton(),
          body: Center(
            // Center is a layout widget. It takes a single child and positions it
            // in the middle of the parent.
            child: 
                Center(child: 
                  FutureBuilder<List<ItemForSale>>(
                    future: itemsForSell,
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        return Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            for(var itemForSale in snapshot.data) 
                              MaterialButton(
                                onPressed: () {
                                  buyItem(itemForSale).then((_) {
                                    Provider.of<PlayerModel>(context, listen: false).addItem(new Item());
                                    Navigator.pop(context);
                                  });
                                },
                                child: Text(itemForSale.item.name + " costs " + itemForSale.price.toString())
                              )
                          ]
                        );
                              
                      } else if (snapshot.hasError) {
                        return Text("${snapshot.error}");
                      }
                      return CircularProgressIndicator();
                    }
                  )
                )
              )
            );
       } );
      }
  }
    

